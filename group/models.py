from django.db import models
from dng.models import *
from UserProfile.models import *


class Group(models.Model):
    name = models.TextField(blank=False)
    description = models.TextField()
    creation_date = models.DateTimeField(auto_now=True)
    creator = models.ForeignKey(User, on_delete=models.CASCADE)

    def counting(self, user):
        dongs = [x.dong for x in GroupDong.objects.filter(gp=self).all()]
        records = Record.objects.filter(dong__in=dongs).all()

        lent = [int(a.amount) for a in records.filter(creditor=user.id, is_paid=False).all()]
        owe = [int(a.amount) for a in records.filter(debtor=user.id, is_paid=False).all()]

        total = sum(lent) - sum(owe)

        return total

    def new_note(self, user):
        group_notes = GroupNote.objects.filter(gp=self).all()

        read_notes = [el.note for el in ReadNote.objects.filter(note__in=group_notes, member=user).all()]
        new_notes = [n for n in group_notes if n not in read_notes]

        new_note_num = len(new_notes)

        return new_note_num

    def get_members(self):
        return [g_member.member for g_member in GroupMember.objects.filter(gp=self).all()]


class GroupDong(models.Model):
    dong = models.OneToOneField(Dong, on_delete=models.CASCADE)
    gp = models.ForeignKey(Group, on_delete=models.CASCADE)


class GroupMember(models.Model):
    member = models.ForeignKey(User, on_delete=models.CASCADE)
    gp = models.ForeignKey(Group, on_delete=models.CASCADE)
    join_date = models.DateTimeField(auto_now=True)


class GroupNote(models.Model):
    gp = models.ForeignKey(Group, on_delete=models.CASCADE)
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    note = models.TextField(blank=False)
    creation_date = models.DateTimeField(auto_now=True)
    attachment = models.FileField(blank=True, null=True)


class ReadNote(models.Model):
    note = models.ForeignKey(GroupNote, on_delete=models.CASCADE)
    member = models.ForeignKey(User, on_delete=models.CASCADE)
