from django.conf.urls import url

from group import views

app_name = 'group'
urlpatterns = [
    url('create', views.create, name='create'),
    url('add_member', views.add_member, name='add_member'),
    url('settle_up', views.settle_up_group, name='settle_up'),
    url('update', views.update, name='update'),
    url('read_note', views.read_note, name='read_note'),
    url('new_note', views.new_note, name='new_note'),
    url('get_members', views.get_members, name='get_members'),

    url('', views.index, name='index'),
]
