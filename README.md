# Dong
An application to manage bills and shares between friends and families.

## requirements
python3.6

## Installation
*developing with virtualenv is prefered :)*

after cloning project:
```
pip install -r requirements.txt
python mnage.py makemigrations
python manage.py migrate
```
after that follow instructions here:

http://django-allauth.readthedocs.io/en/latest/installation.html#post-installation

create Site with id of 1
