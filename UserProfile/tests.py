from django.contrib.auth import get_user_model
from django.test import TestCase
from django.test import Client
from django.urls import reverse


class UserTestCase(TestCase):
    def setUp(self):
        User = get_user_model()
        self.user = User.objects.create_user(username='silly', email='silly@a.com', password='silly')
        self.client = Client()
        logged_in = self.client.login(username='silly', email='silly@a.com', password='silly')
        self.assertEqual(logged_in, True)

        self.user = User.objects.create_user(username='silly2', email='silly2@a.com', password='silly')

    def test_get_users(self):
        url = reverse('UserProfile:getusers')
        res = self.client.get(url).json()
        self.assertEqual(len(res), 1)

    def test_add_friend_wrong_method(self):
        url = reverse('UserProfile:addfriend')
        res = self.client.get(url)
        self.assertRedirects(res, '/')

    def test_add_friend_wrong_format(self):
        url = reverse('UserProfile:addfriend')
        res = self.client.post(url, {'addfriend-auto': 'wrong'})
        self.assertRedirects(res, '/')

