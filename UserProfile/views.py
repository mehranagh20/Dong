from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse

from UserProfile.models import User, Relation
from dng.models import Record, dng, Q
from group.models import Notification, NotificationType


@login_required
def get_users(request):
    users = User.objects.all()
    res = {}
    for user in users:
        if user.username != request.user.username:
            key = "%s (%s)" % (user.username, user.email)
            res[key] = 'http://localhost:8000' + str(user.get_picture())
    return JsonResponse(res)


@login_required
def addfriend(request):
    if request.method == 'POST':
        req = request.POST['addfriend-auto']
        try:
            mail = req.split()[1][1:-1]

        except:
            return HttpResponseRedirect(reverse('homepage'))

        user = get_object_or_404(User, email=mail)
        Relation.objects.create(friend1=request.user, friend2=user)

    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@login_required
def get_friend_detail(request, id):
    if request.method == 'GET':
        res = dict()
        fr = get_object_or_404(User, pk=id)
        user = request.user
        user_owes = user.get_owes()
        user_credits = user.get_credits()

        if fr not in user_owes:
            user_owes[fr] = 0

        if fr not in user_credits:
            user_credits[fr] = 0

        res['settle'] = False
        if user_owes[fr] == user_credits[fr]:
            res['settle'] = True
        else:
            dongs = [{'dong': rec.dong, 'counting': rec.dong.counting(user)} for rec in Record.objects.filter((Q(creditor=fr) & Q(debtor=user))
                                                               | (Q(creditor=user) & Q(debtor=fr)), is_paid=False).order_by('-creation_date')]
            res['dongs'] = dongs
        res['friend'] = fr
        res['balance'] = user_credits[fr] - user_owes[fr]

        return render(request, 'user/friendDetail.html', {'res': res})


def settle_up(request):
    if request.method == 'POST':
        id = request.POST['friend_email']
        fr = User.objects.get(email=id)
        recs = [rec for rec in Record.objects.filter((Q(creditor=fr) & Q(debtor=request.user))
                                                               | (Q(creditor=request.user) & Q(debtor=fr)), is_paid=False).order_by('-creation_date')]

        amount = 0
        for rec in recs:
            amount += rec.amount
            rec.is_paid = True
            rec.save()

        Notification.objects.create(
            person=fr,
            type=NotificationType.settle_up.value,
            amount=amount,
            notifier=request.user
        )
        Notification.objects.create(
            person=request.user,
            type=NotificationType.settle_up.value,
            amount=amount,
            notifier=fr
        )
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
