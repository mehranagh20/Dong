var domain = 'localhost:8000';

var toastHTML = '<span>Please correct the errors and try again</span><a href=""><button class="btn-flat toast-action">reload page</button></a>';
var create_dong_req_type;

var onShow = function () {
    $("#dong-descriptiono").focus();
    $("#dong-description").focus();
    $("#name").focus();
    $("#addfriend-auto").focus();
    $("#group-description").focus();
};

function openDiscovery() {
    $(document).ready(function(){
        // $('.tap-target').tapTarget();
        $('.tap-target').tapTarget('open');
    });


}


function form_validity(name) {
    var form = document.forms[name];
    if (!form.checkValidity())
        M.toast({html: toastHTML, displayLength: 2700});
}


$(document).ready(function () {
    M.AutoInit();

    $.getJSON('http://' + domain + '/users/getusers', null, function (res) {
        console.log(res);
        $(document).ready(function () {
            $('input.autocomplete').autocomplete({
                data: res
            });
        });

    });

    CreateDongGroup();
    CreateDongGroup_group();
    // $(document).ready(function(){
    //  $('.tooltipped').tooltip();
    // });

    $(document).ready(function () {
        $('.tooltipped').tooltip({});

    });


    var elems = document.querySelectorAll('.fixed-action-btn');

    var instances = M.FloatingActionButton.init(elems, {
        hoverEnabled: false
    });

    elems = document.querySelectorAll('.modal');
    instances = M.Modal.init(elems, {
        onOpenEnd: onShow

    });

    // $('#numb').formSelect();
    // $('#saveBut').click(function(){
    //     var instance = M.FormSelect.getInstance($('#numb'));
    //     var _d = instance.getSelectedValues();
    //     console.log(_d);
    // });


});

function myFunction() {
    var instance = M.FormSelect.getInstance($('#numb'));
    var instance2 = document.getElementById('numb2');
    var form1 = document.getElementById('frm1');
    var _d = instance.getSelectedValues();
    var optionArray =[form1.elements[0].value];
    optionArray = optionArray.concat(_d.toString().split(","));
    console.log(optionArray)
    //console.log(instance2.options);
    console.log(form1.elements[0].value);
    instance2.innerHTML = "";

    var input_div = document.getElementById("inputs_for_not_equally");
    input_div.innerHTML = "";
    var x = document.createElement("INPUT");

    for (var o in optionArray) {
        var newOption = document.createElement("option");
        newOption.value = optionArray[o];
        newOption.innerHTML = optionArray[o];
        instance2.options.add(newOption);

        var x = document.createElement("INPUT");
        x.setAttribute("type", "text");
        x.setAttribute("value", "");
        x.setAttribute("id", "input," + newOption.value);
        x.setAttribute("name", "input," + newOption.value);
        x.setAttribute("disabled", "true");
        x.setAttribute("oninput" , "CalSumOfPrices()");
        input_div.appendChild(x);
        input_div.innerHTML += "<label for=" + "input," + newOption.value + ">" + newOption.value + "</label>";
    }
    CalEquallyPrice();
    MakeDongNotEqually();

    $('select').formSelect();

}



function CalSumOfPrices() {
    var instance = M.FormSelect.getInstance($('#numb'));
    var _d = instance.getSelectedValues();
    var optionArray =[document.getElementById('frm1').elements[0].value];
    optionArray = optionArray.concat(_d.toString().split(","));
    var sum = 0;
    for(var o in optionArray) {
        if(document.getElementById("input," + optionArray[o]).value != "")
            sum += parseInt(document.getElementById("input," + optionArray[o]).value);
    }
    var left = "Left :" + ((parseInt(document.getElementById("price").value)) - sum).toString();
    document.getElementById('sum_label').innerHTML = left;
    console.log("12 sum" + left);

    $('input#input_text, textarea#textarea2').characterCounter();
    $('select').formSelect();
}


function validateMyForm() {
    console.log("here")
    var instance = M.FormSelect.getInstance($('#numb'));
    var _d = instance.getSelectedValues();
    var optionArray =[document.getElementById('frm1').elements[0].value];
    optionArray = optionArray.concat(_d.toString().split(","));
    var sum = 0;
    for(var o in optionArray) {
        if(document.getElementById("input," + optionArray[o]).value != "")
            sum += parseInt(document.getElementById("input," + optionArray[o]).value);
    }
    if(((parseInt(document.getElementById("price").value)) - sum) != 0  && document.getElementById("not_equally").checked == true) {
        M.toast({html: '<span>Left must be zero</span><a href=""><button class="btn-flat toast-action">reload page</button></a>'
, displayLength: 2700});
        return false;
    }
    else {
        console.log(10);
        //return true;
    }
}





function CreateDongGroup() {
    //document.getElementById("groupcheck").disabled = false;

        if(document.getElementById("gcheckbox") != null && document.getElementById("groupcheck") != null) {
            if (document.getElementById("gcheckbox").checked == true) {
                document.getElementById("gcheckbox").value = "true";
                document.getElementById("groupcheck").disabled = false;
            }
            else {
                document.getElementById("gcheckbox").value = "false";
                document.getElementById("groupcheck").disabled = true;
            }
        }
    $('select').formSelect();

}

function MakeDongNotEqually() {

    var instance = M.FormSelect.getInstance($('#numb'));
    var _d = instance.getSelectedValues();
    var optionArray =[document.getElementById('frm1').elements[0].value];
    optionArray = optionArray.concat(_d.toString().split(","));

    if(document.getElementById("not_equally").checked == true) {

        for(var o in optionArray)
            document.getElementById("input," + optionArray[o]).disabled = false;

    }
    else {
         for(var o in optionArray)
            document.getElementById("input," + optionArray[o]).disabled = true;
        //document.getElementById('frm1').formNoValidate = false;
    }


    $('input#input_text, textarea#textarea2').characterCounter();
    $('select').formSelect();

}


function CalEquallyPrice() {
    var instance = M.FormSelect.getInstance($('#numb'));
    var _d = instance.getSelectedValues();
    var optionArray =[document.getElementById('frm1').elements[0].value];
    optionArray = optionArray.concat(_d.toString().split(","));
    var n = (optionArray).length;
    for(var o in optionArray)
        document.getElementById("input," + optionArray[o]).placeholder = (parseInt(document.getElementById("price").value) / n).toString();
    $('input#input_text, textarea#textarea2').characterCounter();
    $('select').formSelect();
}

function CalEquallyPrice_group() {
    var instance = M.FormSelect.getInstance($('#numb'));
    var _d = instance.getSelectedValues();
    var optionArray =[document.getElementById('frm1').elements[0].value];
    optionArray = optionArray.concat(_d.toString().split(","));
    var n = (optionArray).length;
    for(var o in optionArray)
        document.getElementById("input," + optionArray[o]).placeholder = (parseInt(document.getElementById("price").value) / n).toString();
    $('input#input_text, textarea#textarea2').characterCounter();
    $('select').formSelect();
}



var SeenNotification = function(){
    $.ajax({
        url: 'http://' + domain + '/dng/seen_notification',
        data: {
            'user_id': $('#user_id_notification').val()
        },
        type: 'GET',
        success: function(result) {
            $("#notification-icon").addClass('text_icons').removeClass('primary_text');
        }
    })
}

function myFunction_group() {
    var instance = M.FormSelect.getInstance($('#numb_g'));
    var instance2 = document.getElementById('numb2_g');
    var form1 = document.getElementById('frm1_group');
    var _d = instance.getSelectedValues();
    var optionArray =[form1.elements[0].value];
    optionArray = optionArray.concat(_d.toString().split(","));
    console.log(optionArray)
    //console.log(instance2.options);
    console.log(form1.elements[0].value);
    instance2.innerHTML = "";

    var input_div = document.getElementById("inputs_for_not_equally_g");
    input_div.innerHTML = "";
    var x = document.createElement("INPUT");

    for (var o in optionArray) {
        var newOption = document.createElement("option");
        newOption.value = optionArray[o];
        newOption.innerHTML = optionArray[o];
        instance2.options.add(newOption);

        var x = document.createElement("INPUT");
        x.setAttribute("type", "text");
        x.setAttribute("value", "");
        x.setAttribute("id", "input," + newOption.value);
        x.setAttribute("name", "input," + newOption.value);
        x.setAttribute("disabled", "true");
        x.setAttribute("oninput" , "CalSumOfPrices_group()");
        input_div.appendChild(x);
        input_div.innerHTML += "<label for=" + "input," + newOption.value + ">" + newOption.value + "</label>";
    }
    CalEquallyPrice_group();
    MakeDongNotEqually_group();

    $('select').formSelect();

}

function CalSumOfPrices_group() {
    var instance = M.FormSelect.getInstance($('#numb_g'));
    var _d = instance.getSelectedValues();
    var optionArray =[document.getElementById('frm1_group').elements[0].value];
    optionArray = optionArray.concat(_d.toString().split(","));
    var sum = 0;
    for(var o in optionArray) {
        if(document.getElementById("input," + optionArray[o]).value != "")
            sum += parseInt(document.getElementById("input," + optionArray[o]).value);
    }
    var left = "Left :" + ((parseInt(document.getElementById("price_g").value)) - sum).toString();
    document.getElementById('sum_label_g').innerHTML = left;
    console.log("12 sum" + left);

    $('input#input_text, textarea#textarea2').characterCounter();
    $('select').formSelect();
}


function validateMyForm_group() {
    console.log("here")
    var instance = M.FormSelect.getInstance($('#numb_g'));
    var _d = instance.getSelectedValues();
    var optionArray =[document.getElementById('frm1_group').elements[0].value];
    optionArray = optionArray.concat(_d.toString().split(","));
    var sum = 0;
    document.getElementById("groupcheck_g").disabled = false;
    document.getElementById("gcheckbox_g").disabled = false;
    for(var o in optionArray) {
        if(document.getElementById("input," + optionArray[o]).value != "")
            sum += parseInt(document.getElementById("input," + optionArray[o]).value);
    }
    if(((parseInt(document.getElementById("price_g").value)) - sum) != 0  && document.getElementById("not_equally_g").checked == true) {
        M.toast({html: '<span>Left must be zero</span><a href=""><button class="btn-flat toast-action">reload page</button></a>'
, displayLength: 2700});
        return false;
    }
    else {
        console.log(10);
        //return true;
    }
}


function CreateDongGroup_group() {
    if(document.getElementById("gcheckbox_g") != null) {
        document.getElementById("gcheckbox_g").checked = true;
        document.getElementById("gcheckbox_g").disabled = true;
    }
    if(document.getElementById("groupcheck_g") != null)
        document.getElementById("groupcheck_g").disabled = true;

    // if(document.getElementById("gcheckbox_g").checked == true) {
    //     document.getElementById("gcheckbox_g").value = "true";
    //     document.getElementById("groupcheck_g").disabled = false;
    // }
    // else {
    //     document.getElementById("gcheckbox_g").value = "false";
    //     document.getElementById("groupcheck_g").disabled = true;
    // }
    $('select').formSelect();
}

function MakeDongNotEqually_group() {

    var instance = M.FormSelect.getInstance($('#numb_g'));
    var _d = instance.getSelectedValues();
    var optionArray =[document.getElementById('frm1_group').elements[0].value];
    optionArray = optionArray.concat(_d.toString().split(","));

    if(document.getElementById("not_equally_g").checked == true) {

        for(var o in optionArray)
            document.getElementById("input," + optionArray[o]).disabled = false;

    }
    else {
         for(var o in optionArray)
            document.getElementById("input," + optionArray[o]).disabled = true;
        //document.getElementById('frm1_group').formNoValidate = false;
    }


    $('input#input_text, textarea#textarea2').characterCounter();
    $('select').formSelect();

}

function CalEquallyPrice_group() {
    var instance = M.FormSelect.getInstance($('#numb_g'));
    var _d = instance.getSelectedValues();
    var optionArray =[document.getElementById('frm1_group').elements[0].value];
    optionArray = optionArray.concat(_d.toString().split(","));
    var n = (optionArray).length;
    for(var o in optionArray)
        document.getElementById("input," + optionArray[o]).placeholder = (parseInt(document.getElementById("price_g").value) / n).toString();
    $('input#input_text, textarea#textarea2').characterCounter();
    $('select').formSelect();
}
