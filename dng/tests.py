from django.contrib.auth import get_user_model
from django.test import TestCase
from django.test import Client
from django.urls import reverse


class DongTestCase(TestCase):
    def setUp(self):
        User = get_user_model()
        self.user = User.objects.create_user(username='silly', email='silly@a.com', password='silly')
        self.client = Client()
        logged_in = self.client.login(username='silly', email='silly@a.com', password='silly')
        self.assertEqual(logged_in, True)

    def test_index(self):
        url = reverse('dng:index')
        res = self.client.get(url)
        self.assertTemplateUsed(res, 'index.html')

    # def test_add_friend_wrong_method(self):
    #     url = reverse('UserProfile:addfriend')
    #     res = self.client.get(url)
    #     self.assertRedirects(res, '/')
    #
    # def test_add_friend_wrong_format(self):
    #     url = reverse('UserProfile:addfriend')
    #     res = self.client.post(url, {'addfriend-auto': 'wrong'})
    #     self.assertRedirects(res, '/')
#