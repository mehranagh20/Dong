from ..models import Record
from django import template
register = template.Library()


@register.inclusion_tag('dong/dong_info.html')
def dong_info(dong):
    records = Record.objects.filter(dong=dong).all()
    members = {x.debtor for x in Record.objects.filter(dong=dong).all()}.union({x.creditor for x in records})
    return {
        'dong': dong,
        'dong_total': sum([rec.amount for rec in records]),
        'records': records,
        'participants': [{'user': u, 'paid': dong.get_pays(u), 'owes': dong.get_owes(u)} for u in members]
    }
