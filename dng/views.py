from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from group.models import *
from django.utils import timezone
from django import template
register = template.Library()


@login_required
def create(request):
    if request.method == 'POST':
        dong_obj = Dong.objects.create(
            owner=request.user,
            name=request.POST['name'],
            creation_date=timezone.now(),
        )

        participants = request.POST.getlist('numb')
        participants.append(request.user.username)

        amount = 0
        payer = request.POST.getlist('numb2')
        payer = User.objects.get(username=payer[0])
        for participant in participants:
            debtor = User.objects.get(username=participant)
            if "not_equally" in request.POST:
                mon = int(request.POST['input,' + participant])
            else:
                mon = int(request.POST['price'])/len(participants)
            if debtor.id != payer.id:
                rec = Record.objects.create(
                    dong=dong_obj,
                    creditor=payer,
                    debtor=debtor,
                    amount=mon,
                    pay_date=timezone.now(),
                )
                amount += rec.amount

        if "gcheckbox" in request.POST:
            gps = request.POST.getlist('groupcheck')
            GroupDong.objects.create(
                dong=dong_obj,
                gp=Group.objects.get(pk=int(gps[0])),
            )

        for participant in participants:
            Notification.objects.create(
                person=User.objects.get(username=participant),
                type=NotificationType.dong.value,
                amount=amount,
                notifier=request.user,
                dong=dong_obj
            )

    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


def index(request):
    return render(request, 'index.html')


def seen_notification(request):
    user = User.objects.filter(pk=request.GET['user_id']).first()

    notifications = Notification.objects.filter(person=user).filter(seen=False).all()

    for notification in notifications:
        notification.seen = True
        notification.save()

    return JsonResponse({'success': True})
